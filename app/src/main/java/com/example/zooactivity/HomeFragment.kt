package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding : FragmentHomeBinding? = null
    private val binding : FragmentHomeBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentHomeBinding.inflate(layoutInflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding){
        btnAnimalOne.setOnClickListener{
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalOneFragment("name","ipsum")
            findNavController().navigate(action)
        }
        btnAnimalTwo.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalTwoFragment("name","lorem")
            findNavController().navigate(action)
        }
        btnAnimalThree.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalThreeFragment("name","figuro")
            findNavController().navigate(action)
        }
        btnAnimalFour.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalFourFragment("name","vidi")
            findNavController().navigate(action)
        }
    }
}