package com.example.zooactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.navigation.navArgs
import java.util.logging.Logger

class ZooActivity : AppCompatActivity(R.layout.activity_zoo){
    private val args by navArgs<ZooActivityArgs>()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        val toaster = Toast.makeText(this,"Welcome to " + args.zooName,Toast.LENGTH_SHORT)
        toaster.show()
        Log.d("welcome","welcome to " + args.zooName)

    }
}
