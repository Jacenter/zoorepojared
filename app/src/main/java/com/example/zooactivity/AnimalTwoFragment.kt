package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.zooactivity.databinding.FragmentAnimalTwoBinding

class AnimalTwoFragment : Fragment() {
    private var _binding : FragmentAnimalTwoBinding? = null
    private val binding: FragmentAnimalTwoBinding get() = _binding!!
    private val args by navArgs<AnimalTwoFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentAnimalTwoBinding.inflate(inflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() = with(binding) {
        nameViewDisplay.text = args.animalName
        detailViewDisplay.text = args.animalDetails

    }
}