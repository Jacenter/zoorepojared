package com.example.zooactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {
    private var _binding: FragmentFinishBinding? = null
    private val binding: FragmentFinishBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentFinishBinding.inflate(inflater, container, false).also {
            _binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnBackToGetStartFragment.setOnClickListener {
            val action = FinishFragmentDirections.actionFinishFragmentToGettingStartedFragment()
            findNavController().navigate(action)
        }
        btnFinish.setOnClickListener {
            val action = FinishFragmentDirections.actionFinishFragmentToZooActivity("Jared's Zoo")
            findNavController().navigate(action)

        }
    }

}