package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.zooactivity.databinding.FragmentAnimalOneBinding

class AnimalOneFragment : Fragment() {
    private var _binding: FragmentAnimalOneBinding? = null
    private val binding: FragmentAnimalOneBinding get() = _binding!!
    private val args by navArgs<AnimalOneFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentAnimalOneBinding.inflate(inflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() = with(binding) {
        nameViewDisplay.text = args.animalName
        detailViewDisplay.text = args.animalDetails

    }

}