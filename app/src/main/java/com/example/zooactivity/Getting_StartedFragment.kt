package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentGettingStartedBinding
import com.example.zooactivity.databinding.FragmentHelloBinding


class Getting_StartedFragment : Fragment() {
    private var _binding: FragmentGettingStartedBinding? = null
    private val binding: FragmentGettingStartedBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentGettingStartedBinding.inflate(layoutInflater, container, false).also {
            _binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnBackToHelloFragment.setOnClickListener {
            val action = Getting_StartedFragmentDirections.actionGettingStartedFragmentToHelloFragment()
            findNavController().navigate(action)
        }
        btnNextGoToFinishFragment.setOnClickListener{
            val action = Getting_StartedFragmentDirections.actionGettingStartedFragmentToFinishFragment()
            findNavController().navigate(action)
        }


    }
}