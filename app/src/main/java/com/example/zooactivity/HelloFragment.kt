package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentHelloBinding


class HelloFragment : Fragment() {

    private var _binding: FragmentHelloBinding? = null
    private val binding: FragmentHelloBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentHelloBinding.inflate(layoutInflater, container, false).also {
            _binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding){
        btnNext.setOnClickListener{
            val action = HelloFragmentDirections.actionHelloFragmentToGettingStartedFragment()
            findNavController().navigate(action)
        }
    }
}