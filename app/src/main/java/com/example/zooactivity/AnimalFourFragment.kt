package com.example.zooactivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.zooactivity.databinding.FragmentAnimalFourBinding

class AnimalFourFragment : Fragment() {
    private var _binding: FragmentAnimalFourBinding? = null
    private val binding: FragmentAnimalFourBinding get() = _binding!!
    private val args by navArgs<AnimalFourFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentAnimalFourBinding.inflate(layoutInflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() = with(binding) {
        nameViewDisplay.text = args.animalName
        detailViewDisplay.text = args.animalDetails

    }
}